import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressRouterModule } from '@ngx-progressbar/router';
import { NotfoundComponent } from './notfound/notfound.component';
import { HomeComponent } from './home/home.component';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { CategoryComponent } from './category/category.component';
import { WpService } from './services/wp.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AuthorComponent } from './author/author.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoryListComponent } from './category-list/category-list.component';
import { ListPostsComponent } from './list-posts/list-posts.component';
import { OfflineComponent } from './offline/offline.component';
import { Browser } from 'protractor';
import { PageComponent } from './page/page.component';
import { ShareComponent } from './post-detail/share/share.component';
import { TagListComponent } from './tag-list/tag-list.component';
import { TagComponent } from './tag/tag.component';
import { SpecialListComponent } from './special-list/special-list.component';
import { StorageService } from './services/storage.service';


@NgModule({
  declarations: [
    AppComponent,
    NotfoundComponent,
    HomeComponent,
    PostDetailComponent,
    CategoryComponent,
    AuthorComponent,
    CategoryListComponent,
    ListPostsComponent,
    OfflineComponent,
    PageComponent,
    ShareComponent,
    TagListComponent,
    TagComponent,
    SpecialListComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgProgressModule,
    NgProgressRouterModule,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    AppRoutingModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [WpService, StorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
