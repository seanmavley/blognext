import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { WpService } from '../services/wp.service';
import { ngListTransition } from '../utils/transition';
import { StorageService } from '../services/storage.service';

declare let UIkit: any;

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css'],
  animations: [ngListTransition]
})
export class PostDetailComponent implements OnInit {

  busy: boolean;
  user: any;
  post: any;
  side_busy: boolean;
  merged_array: any;
  error: boolean;
  post_id: any;

  is_saved: boolean;

  constructor(private title: Title,
    private route: ActivatedRoute,
    private sts: StorageService,
    private wp: WpService) { }

  ngOnInit() {
    const slug = this.route.snapshot.params['slug'];
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.post_id = params.get('post_id');
      this.loadPost(this.post_id);
    });
  }

  loadPost(post_id) {
    this.busy = true;
    this.wp.get_post(post_id)
      .subscribe((res) => {
        // console.log(res);
        this.post = res;

        this.busy = false;
        this.title.setTitle(res['title'].rendered);
        this.sts.save_post(this.post.id, this.post);

        this.is_saved = this.sts.is_post_local(this.post.id);

        this.loadUser(res['author']);

        const category_id = res['_embedded']['wp:term'][0];
        const tag_id = res['_embedded']['wp:term'][1];
        this.loadCategoryPosts(category_id, tag_id);
      }, (error) => {
        if (this.sts.is_post_local(post_id)) {
          this.post = this.sts.get_post(post_id); // get post from localstorage
          this.busy = false;
        } else {
          this.error = true;
        }
      });
  }

  loadUser(id) {
    this.wp.get_user(id)
      .subscribe((res) => {
        this.user = res;
        // console.log(res);
      });
  }

  loadTagPosts(tag_id, cat_array) {
    // Possible to have no tag assigned.
    if (tag_id.length) {
      this.wp.get_tag_posts(tag_id[0].id, 1, 10)
        .subscribe((res) => {
          this.doMerge(res, cat_array);
        });
    } else {
      this.merged_array = cat_array;
      this.side_busy = false;
    }
  }

  loadCategoryPosts(category_id, tag_id) {
    this.side_busy = true;
    this.wp.get_category_posts(category_id[0].id, 1, 10)
      .subscribe((res) => {
        this.loadTagPosts(tag_id, res);
      });
  }

  doMerge(tag_array, cat_array) {
    for (let i = 0, l = tag_array.length; i < l; i++) {
      for (let j = 0, ll = cat_array.length; j < ll; j++) {
        if (tag_array[i].slug === cat_array[j].slug) {
          tag_array.splice(i, 1, cat_array[j]);
          break;
        }
      }
    }

    this.side_busy = false;
    this.merged_array = tag_array;

    // console.log(tag_array);
  }

  hideModal() {
    UIkit.modal('#user').hide();
  }

  doRefresh(event) {
    // console.log(event);
    this.loadPost(this.post_id);
    this.error = false;
  }

}
