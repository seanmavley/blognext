import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { WpService } from '../services/wp.service';
import { StorageService } from '../services/storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {

  error: boolean;
  end: boolean;
  posts: any;
  busy: boolean;

  constructor(private title: Title, private wp: WpService, private sts: StorageService) { }

  ngOnInit() {
    this.title.setTitle(`KhoPhi's Blog`);
    this.loadPosts();
  }

  loadPosts() {
    this.busy = true;
    this.wp.get_posts(20)
      .subscribe((res) => {
        // console.log(res);
        this.posts = res;
        this.busy = false;
        this.sts.save_posts(this.posts);
      }, (error) => {
        if (this.sts.get_posts) {
          this.posts = this.sts.get_posts();
          console.log(this.posts);
          this.busy = false;
          return;
        }
        this.error = true;
      });
  }

  doRefresh(event) {
    console.log(event);
    this.loadPosts();
    this.error = false;
  }

}
